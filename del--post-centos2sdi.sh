#!/bin/bash

curl -s https://gitlab.com/amitkarpe/a/raw/master/sevone/download-sdi.sh > download-sdi.sh; chmod +x download-sdi.sh; sudo ./download-sdi.sh $1

curl -s https://gitlab.com/amitkarpe/a/raw/master/sevone/k8scentos.sh > k8scentos.sh; chmod +x k8scentos.sh; sudo ./k8scentos.sh

yum info kubelet-1.8.10 kubeadm-1.8.10 kubectl-1.8.10 | grep Version -B 2
docker info | grep -E 'Storage Driver|Server Version'

sdi-cli cluster provision -y  --single-node --size xsmall 