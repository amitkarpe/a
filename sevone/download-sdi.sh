#!/bin/bash

if [ $# -ne 1 ]
  then
        echo "$#"
    echo "Please provide SDI version to install. Like 1.5.0 or 1.5.1 or 1.5.2 or 1.5.3 or 1.6.0";
  else
    version="${1}"
    echo "Installing SevOne Data Insight version ${version}"; echo "Going to EXIT"; exit;
fi

which wget > /dev/null 2>&1 
if [ $? -eq 0 ] 
  then
    echo ""
  else
    echo "install wget"
    sudo yum -y install wget
    echo ""
fi

function gdrive_download () {
  CONFIRM=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p');
  wget --no-check-certificate --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2;
  rm -rf /tmp/cookies.txt;
}
echo "" > download-sdi.log

function download () {
  filename="$1"
  fileid="$2"
  echo "Downloading $filename"
  if [ ! -f $filename ] 
  then
    gdrive_download $fileid $filename >> download-sdi.log 2>&1 
    echo "Successfully downloaded";  echo "Please check its md5sum.";
  else
    echo "File already exist."
    ls -lh $filename
    echo ""
  fi
  echo ""
}

yum install -y vim telnet tmux wget tmux nano unzip traceroute wget git net-tools bind-utils yum-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct ansible

filename='helm-v2.7.2-linux-amd64.tar.gz'
fileid='1ZdudIaRq18t98EdgLiEOHvw9wMTB9Scg'
download $filename $fileid;

filename='kube-image-deps-v1.8.10.tgz'
fileid='1eb5nTu6YtbNsDptB34iX7uXKdZNFCg-O'
download $filename $fileid;

filename='nginx-ingress-0.10.2.tgz'
fileid='1Ha3uWH_-tvL-EBQ8UsmcM4IscTTTG7l1'
download $filename $fileid;

# if [ "${version}" = "1.5.0" || "${version}" = "1.5.1" || "${version}" = "1.5.2" || "${version}" = "1.5.3" ]
if [ "${version}" = "1.5.0" ] || [ "${version}" = "1.5.1" ] || [ "${version}" = "1.5.2" ] || [ "${version}" = "1.5.3" ]
then
  if [ ! -f ins1ght-installer-v${version}.tgz ] 
  then
    wget https://docker.sevone.com/docker-image/retina/installer/v${version}/ins1ght-installer-v${version}.tgz
  fi
fi

if [ "${version}" = "1.5.0" ] || [ "${version}" = "1.5" ]
then
  if [ ! -f ins1ght-installer-v${version}.tgz ] 
  then
    version="1.5"
    wget https://docker.sevone.com/docker-image/retina/installer/v${version}/ins1ght-installer-v${version}.tgz    
  fi
fi


if [ "${version}" = "1.6" ] || [ "${version}" = "1.6.0" ]
then
  if [ ! -f ins1ght-installer-v1.6.0.tgz ] 
  then  
    wget http://artifactory01.devops.sevone.com/docker-image-staging/retina/installer/v1.6.0/ins1ght-installer-v1.6.0.tgz
  fi
fi

echo ""
curl -s https://gitlab.com/amitkarpe/a/raw/master/sevone/md5sum > md5sum
echo "Checking md5sum for files"
echo ""
md5sum -c md5sum
echo ""

sudo mkdir /opt/datainsight; sudo chown sevone:sevone -R /opt/datainsight; 
sudo groupadd docker; sudo usermod -aG docker $USER
sudo usermod -aG dockerroot $USER
tar zxvf ins1ght-installer-v${version}.tgz -C /opt/datainsight; cd /opt/datainsight; sudo make install
tar zxvf helm-v2.7.2-linux-amd64.tar.gz; sudo cp -v linux-amd64/helm /usr/bin/

#sudo mkdir -p /opt/helm-chart-deps/nginx-ingress-0.10.2; sudo tar zxvf /home/sevone/nginx-ingress-0.10.2.tgz -C /opt/helm-chart-deps/nginx-ingress-0.10.2/
#sudo mkdir -p /opt/kube-image-deps/; sudo cp /home/sevone/kube-image-deps-v1.8.10.tgz /opt/kube-image-deps/
#wget http://artifactory01.devops.sevone.com/app-image-staging/SDI/python-deps/python-deps-v1.4.tgz
#sudo mkdir -p /opt/python-deps/; sudo tar zxvf python-deps-v1.4.tgz -C /opt/python-deps/
