cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF


cat <<EOF >>  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

systemctl disable firewalld && systemctl stop firewalld
sestatus
getenforce
#setenforce 0
setenforce permissive
#echo "SELINUX=disabled" > /etc/sysconfig/selinux
echo "SELINUX=permissive" > /etc/sysconfig/selinux
swapoff -a
sed -e '/swap/ s/^#*/#/' -i /etc/fstab

sysctl --system
#yum install -y kubelet kubeadm kubectl docker
yum install -y docker-1.13.1 kubelet-1.8.10 kubeadm-1.8.10 kubectl-1.8.10 kubernetes-cni-0.5.1-1.x86_64 vim telnet tmux wget tmux nano unzip traceroute wget git net-tools bind-utils yum-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct ansible
systemctl enable kubelet && systemctl start kubelet
systemctl enable docker && systemctl start docker

groupadd docker;  
usermod -aG docker sevone; 
usermod -aG dockerroot sevone;

# yum update --exclude=docker* --exclude=kernel* --exclude=kube* --exclude=docker* --exclude=python*
echo "Add - exclude=docker* exclude=kernel* exclude=kube* exclude=docker* exclude=python* "
read
#sed -i -e 's/enabled=1/enabled=0/g' /etc/yum.conf
vim /etc/yum.conf

grep -i dock /etc/group
echo "Add - sevone ALL=NOPASSWD: /usr/bin/kubeadm*,/usr/bin/rm -rf /var/sdi-data,/usr/bin/cp,/usr/bin/chown,/usr/bin/systemctl"
read
sudo visudo

systemctl restart docker

ln -s /usr/bin/kubectl /usr/bin/k -v
yum info kubelet-1.8.10 kubeadm-1.8.10 kubectl-1.8.10 docker-1.13.1 | grep Version -B 2
docker info | grep -E 'Storage Driver|Server Version'

echo "Please run 'kubeadm init', to initialized the kubernetes cluster"

echo "Please reboot the system"

exit;

kubectl get nodes
kubectl get pods 
kubectl get pods --all-namespaces