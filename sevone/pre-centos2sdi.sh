#!/bin/bash

echo ""
echo "Pre-installation for SDI on CentOS is started."
echo ""
echo "new user as 'sevone' going to be added into the system. "
echo ""
adduser sevone;
if [ $? -eq 0 ]
then
  echo ""
  echo "Successfully created";
  echo ""
  passwd sevone;
else
  echo ""
#  echo "User 'sevone' already exists" >&2
#  echo ""
fi
echo ""
usermod -aG wheel sevone;
if [ $? -eq 0 ]
then
    echo ""
    echo "user sevone added into sudo group"
    echo ""
else
  echo ""
  echo "Please contact administrator to know why user sevone NOT added into 'sudo' group"
  echo ""
fi
echo "What should the hostname?"
read host;
echo "Hostname going set as ${host}";
echo "";
echo "";
hostnamectl set-hostname ${host};
#hostnamectl set-hostname sevone-di15;
echo "System is going to reboot. Please press enter to continue or 'Ctrl + C' to stop it.";
echo ""
echo ""
read;
systemctl reboot